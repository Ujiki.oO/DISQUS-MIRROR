/*	_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
	_/	DISQUS.js	2018/09/14	Version:1.2	by Ujiki.oO
	_/	誰もがDISQUSの利用をミラーリング出来る１つの共通 JavaScript を提供します。
	_/	DISQUSのメニューは Google Translator と連携し多言語表示します。
	_/	【利用手順】：
	_/ 1.	DISQUSで初期登録した管理コードを BASEDISQUSHOSTNAME2018 に定義します！
	_/ 2.	<div id="disqus_thread"></div> を定義しないと処理されません。
	_/ 3.	メインブログ：で呼び出す。メインブログであることの判断は MYMAINBLOGURL2018
	_/	で決めます。
	_/ 4.	その他のブログでは呼び出す前に「PAGE_URL」と「PAGE_IDENTIFIER」を定義する！
	_/ 5.	メインブログには WordPress を意識しており、「設定」＞「パーマリンク設定」＞
	_/	「共通設定」：「投稿名」に選択しています。他の設定時には、
	_/	 PAGE_IDENTIFIER への代入式を検討して下さい！
	_/ 6.	NEWTYPE2018DISQUS グローバル変数を追加しました。
	_/	最初「http:」で開始した WordPress に対して、「https:」に移行した直後から
	_/	NEWTYPE2018DISQUS グローバル変数を true にセットする。
	_/ 7.	イベントリスナー処理を追加しました。
	_/					2018/08/20-2018/09/14 by paypal.me/Ujiki
	_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
if(typeof MYMAINBLOGURL2018 == "undefined"){//
//	あなたのメインブログの location.hostname を定義します。
var	MYMAINBLOGURL2018 = 'motpresse.votre.space';//	メインブログ
}//
if(typeof BASEDISQUSHOSTNAME2018 == "undefined"){//
//	あなたのDISQUSに登録したコードを定義します。
var	BASEDISQUSHOSTNAME2018 = 'motpressevotrespac';//
}//
if(typeof INSERTADJACENTHTML2018 == "undefined"){//
	if(typeof VERSIONOFDISQUS_JS == "undefined")var VERSIONOFDISQUS_JS = '<div align="right"><a href="https://gitlab.com/Ujiki.oO/DISQUS-MIRROR/blob/master/JA/DISQUS.js" target="_blank"><font size=1 color=gray>DISQUS.js Ver:1.0</font></a></div>';//
//	DISQUSの直前に埋め込まれるボタンに表示される文字列です。
var	INSERTADJACENTHTML2018='<a name="disqus_thread"> </a><br clear="all" />';//
	INSERTADJACENTHTML2018+='<a href="#google_translate_element" class="notranslate">Google Translator</a>.  ';// Google Translate を実装していない時は、この行をコメント化します。
	INSERTADJACENTHTML2018+='<input type=button value="あなたの言語でDISQUSメニューを再表示する！" onClick="if(typeof CreateDisqusEmbed2018 == \'function\')CreateDisqusEmbed2018(\''+BASEDISQUSHOSTNAME2018+'\')" class="translate cross_line" />';//
}//
if(typeof CSS4DISQUS2018 == "undefined"){//
//	DISQUSの直前に埋め込まれるボタンとDISQUS本体の修飾です。
var	CSS4DISQUS2018='<style>';//
	CSS4DISQUS2018+=' #disqus_thread {border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; padding:15px;color:navy; background:#CFF;background-color:#CFF;-moz-box-shadow: 10px 10px 11px #7286b3;-webkit-box-shadow: 10px 10px 11px #7286b3;box-shadow: 10px 10px 11px #7286b3;filter: progid:DXImageTransform.Microsoft.Shadow(strength = 10, direction = 135, color = \'#7286b3\');-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(strength = 10, Direction = 135, Color = \'#7286b3\')";}';//
	CSS4DISQUS2018+=' .cross_line{display:inline-block;position:relative;padding:.25em 1em;border-top:solid 2px black;border-bottom:solid 2px black;text-decoration:none;font-weight:bold;color:#03a9f4}.cross_line:before,.cross_line:after{content:\'\';position:absolute;top:-7px;width:2px;height:-webkit-calc(100%+14px);height:calc(100%+14px);background-color:black;transition:.3s}.cross_line:before{left:7px}.cross_line:after{right:7px}.cross_line:hover:before{top:0;left:0;height:100%}.cross_line:hover:after{top:0;right:0;height:100%}';//
	CSS4DISQUS2018+='</style>';//
// Thanks at https://saruwakakun.com/html-css/reference/buttons  !
}//
//	※　以下は修正しないでください　※
if(typeof NEWTYPE2018DISQUS == "undefined")	NEWTYPE2018DISQUS = false;//
if(typeof addOnload2018 == "undefined"){//
function addOnload2018(func,ms){if(ms&&eval(ms)>0){try{window.addEventListener("load",setTimeout(function(){func},ms),false)}catch(e){window.attachEvent("onload",setTimeout(function(){func},ms))}}else{try{window.addEventListener("load",func,false)}catch(e){window.attachEvent("onload",func)}}}// ページのローディング終了後に起動される関数。オプションとして「時間経過後に起動させる」ことも可能。使用例：  関数「ABC」をページローディング完了後５秒を経過したら実行させる：  addOnload2018( ABC , 5000 )
}//
if(location.hostname == MYMAINBLOGURL2018){//
//	ミラー側の記事では「PAGE_URL」と「PAGE_IDENTIFIER」を事前に指定すること！
	if(NEWTYPE2018DISQUS)//
		PAGE_URL = 'https://' + location.hostname + location.pathname ;// 非SSL/SSLハイブリッド対応
	else	PAGE_URL = 'http://' + location.hostname + location.pathname ;//
	PAGE_IDENTIFIER = location.pathname.split('/')[1] ;//
//	メインブログには WordPress を意識しており、「設定」＞「パーマリンク設定」＞
//	「共通設定」：「投稿名」に選択しています。他の設定時には、
//	 PAGE_IDENTIFIER への代入式を検討して下さい！
}
if(typeof _GETELEMENTBYID__disqus_thread_ == "undefined")//
var	_GETELEMENTBYID__disqus_thread_ = document.getElementById('disqus_thread');//
if(typeof WELCOM2THE1STDISQUS2018 == "undefined" && _GETELEMENTBYID__disqus_thread_ ){//	to A
var	WELCOM2THE1STDISQUS2018 = true ;//
	_GETELEMENTBYID__disqus_thread_.insertAdjacentHTML('beforebegin',INSERTADJACENTHTML2018);//
	_GETELEMENTBYID__disqus_thread_.insertAdjacentHTML('afterend',VERSIONOFDISQUS_JS);//
	document.write(CSS4DISQUS2018);//
	CSS4DISQUS2018 = '<noscript><center>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></center></noscript>';//
	if(typeof disqus_config == "undefined"){//	to B
var		disqus_config = function(){//
			function browserLanguage(zz){//
				try {return (navigator.browserLanguage || navigator.language || navigator.userLanguage).substr(0,zz)}//
				catch(e) {return 'en'}//
			}//
var			q = document.cookie.indexOf("googtrans=");//
			if(q >= 0){//
var				qqs = document.cookie.split(';');//
				for( var i = 0 ; i < qqs.length ; i++ ){//
					if( qqs[ i ].indexOf("googtrans=") < 0 ){//
						continue;//
					}//
					q = qqs[ i ].split('/')[2].replace(/ /g,'');//
					break;//
				}//
			}else{	q = browserLanguage(2);//
				switch(q){//
					case"zh":q=browserLanguage(5);break;//中国 zh-CN:Simplified / zh-TW:Traditional
					case"ce":if(browserLanguage(3)=="ceb"){q="ceb"}break;//Cebuano セブアノ語は、オーストロネシア語族, ヘスペロネシア語派, 中央フィリピン諸語, 中部フィリピン語群, 南ビサヤ小語群に属する言語である。
					case"ha":if(browserLanguage(3)=="haw"){q="haw"}break;//Hawaiian ハワイ語は、オーストロネシア語族に属し、ハワイ諸島先住民のポリネシア人であるハワイ人の先祖代々の言語である。英語とともにハワイ州の公用語に指定されている。
					case"hm":q=browserLanguage(3);break//Hmong ミャオ族 は、中国の国内に多く居住する民族集団で、同系統の言語を話す人々は、タイ、ミャンマー、ラオス、ベトナムなどの山岳地帯に住んでいる。自称はモン族 であるが、Hmongは狭義にはミャオ族の一支族に用いられる呼称である。中国では55の少数民族の一つである。
				}//
			}//
			this.language = q ;//
			this.page.url = PAGE_URL ;//
			this.page.identifier = PAGE_IDENTIFIER ;//
			q = null ; qqs = null;//
		}//
		function CreateDisqusEmbed2018(z){//
var			d = document, s = d.createElement('script');//
			s.src = 'https://' + z + '.disqus.com/embed.js';//
			s.setAttribute('data-timestamp', +new Date());//
			(d.head || d.body).appendChild(s);//
			s = null;//
var			s = d.createElement('script');//
			s.src = 'https://' + z + '.disqus.com/count.js';//
			s.setAttribute('id','dsq-count-scr');//
			s.setAttribute('async',true);//
			d.body.appendChild(s);//
//			s = null ; //
		}//
//		CreateDisqusEmbed2018( BASEDISQUSHOSTNAME2018 );//
	}//	from B
	(function(){//
var		bottom , targetTop ; //
		window.addEventListener('scroll', this._scrollIn = function(){//
//			表示領域の下端の位置
			bottom = window.innerHeight + window.pageYOffset ;//
//			要素の上端の位置
			targetTop = _GETELEMENTBYID__disqus_thread_.offsetTop ;//
			if(targetTop < bottom){//
				window.removeEventListener('scroll', this._scrollIn , false);//
				CreateDisqusEmbed2018( BASEDISQUSHOSTNAME2018 );//
			}//
		});//
	})();//
}//	from A
/*
【改訂履歴】：
Ver:1.2 2018/09/14
+ window.addEventListener('scroll',
+ function CreateCountJs2018(z){//

Ver:1.1	2018/08/29
+ if(typeof NEWTYPE2018DISQUS == "undefined")	NEWTYPE2018DISQUS = false;//
+ if(NEWTYPE2018DISQUS)//
+	PAGE_URL = 'https://' + location.hostname + location.pathname ;// 非SSL/SSLハイブリッド対応
+ else	PAGE_URL = 'http://' + location.hostname + location.pathname ;//


Ver:1.0 2018/08/20
多重定義、多重呼び出しでもエラーを回避します。
- if(typeof CSS4DISQUS2018 == "undefined") CSS4DISQUS2018 = '<noscript><center>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></center></noscript>';//
+ CSS4DISQUS2018 = '<noscript><center>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></center></noscript>';//
*/
