# DISQUS-MIRROR

複数のブログやホームページで簡単に間違いなくDISQUSをミラーリングする  JavaScript を公開します。DISQUSのメニューについては  Google翻訳に連動します。  何らかのタイムアウトで表示しないDISQUSを再表示させるボタンが自動的に配置されます。

【詳細解説記事】：
My WordPress:  http://motpresse.votre.space/disqus/#disqus_thread
My MovableType:  http://mt.fs4y.com/mt/disqus-1/disqus.html#disqus_thread
FC2:  http://create2014.blog.fc2.com/blog-entry-466.html#disqus_thread
