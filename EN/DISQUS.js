/*	_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
	_/	DISQUS.js	2018/09/14	Version:1.2	by Ujiki.oO
	_/	Everyone provides one common JavaScript that can mirror the usage of DISQUS.
	_/	DISQUS menu is displayed in multiple languages in cooperation with Google Translator.
	_/	[ Procedure ]:
	_/ 1.	Define the management code initially registered with DISQUS to BASEDISQUSHOSTNAME2018 !
	_/ 2.	<div id = "disqus_thread"> </ div> must be defined before it is processed.
	_/ 3.	Main blog: Call with. The judgment of being the main blog is decided by MYMAINBLOGURL 2018.
	_/ 4.	On other blogs, define "PAGE_URL" and "PAGE_IDENTIFIER" before calling !
	_/ 5.	I am conscious of WordPress on the main blog and I have chosen "setting">
	_/	"permalink setting"> "common setting": "post name". When setting other,
	_/	please consider the assignment expression to PAGE_IDENTIFIER!
	_/ 6.	NEWTYPE 2018 DISQUS global variable was added.
	_/	For WordPress which started with "http:" first,
	_/	set NEWTYPE 2018 DISQUS global variable to true immediately after switching to "https:".
	_/ 7.	Event listener processing was added.
	_/					2018/08/20-2018/09/14 by paypal.me/Ujiki
	_/					https://goo.gl/HGS2y6
	_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
if(typeof MYMAINBLOGURL2018 == "undefined"){//
//	Define location.hostname of your main blog.
var	MYMAINBLOGURL2018 = 'motpresse.votre.space';//	Main blog
}//
if(typeof BASEDISQUSHOSTNAME2018 == "undefined"){//
//	Define the code registered with your DISQUS.
var	BASEDISQUSHOSTNAME2018 = 'motpressevotrespac';//
}//
if(typeof INSERTADJACENTHTML2018 == "undefined"){//
	if(typeof VERSIONOFDISQUS_JS == "undefined")var VERSIONOFDISQUS_JS = '<div align="right"><a href="https://gitlab.com/Ujiki.oO/DISQUS-MIRROR/blob/master/JA/DISQUS.js" target="_blank"><font size=1 color=gray>DISQUS.js Ver:1.0</font></a></div>';//
//	A character string displayed on the button embedded immediately before DISQUS.
var	INSERTADJACENTHTML2018='<a name="disqus_thread"> </a><br clear="all" />';//
	INSERTADJACENTHTML2018+='<a href="#google_translate_element" class="notranslate">Google Translator</a>.  ';// Comment out this line when you do not implement Google Translate.
	INSERTADJACENTHTML2018+='<input type=button value="Redisplay the DISQUS menu in your language !" onClick="if(typeof CreateDisqusEmbed2018 == \'function\')CreateDisqusEmbed2018(\''+BASEDISQUSHOSTNAME2018+'\')" class="translate cross_line" />';//
}//
if(typeof CSS4DISQUS2018 == "undefined"){//
//	It is the modification of the button and DISQUS body embedded immediately before DISQUS.
var	CSS4DISQUS2018='<style>';//
	CSS4DISQUS2018+=' #disqus_thread {border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; padding:15px;color:navy; background:#CFF;background-color:#CFF;-moz-box-shadow: 10px 10px 11px #7286b3;-webkit-box-shadow: 10px 10px 11px #7286b3;box-shadow: 10px 10px 11px #7286b3;filter: progid:DXImageTransform.Microsoft.Shadow(strength = 10, direction = 135, color = \'#7286b3\');-ms-filter: "progid:DXImageTransform.Microsoft.Shadow(strength = 10, Direction = 135, Color = \'#7286b3\')";}';//
	CSS4DISQUS2018+=' .cross_line{display:inline-block;position:relative;padding:.25em 1em;border-top:solid 2px black;border-bottom:solid 2px black;text-decoration:none;font-weight:bold;color:#03a9f4}.cross_line:before,.cross_line:after{content:\'\';position:absolute;top:-7px;width:2px;height:-webkit-calc(100%+14px);height:calc(100%+14px);background-color:black;transition:.3s}.cross_line:before{left:7px}.cross_line:after{right:7px}.cross_line:hover:before{top:0;left:0;height:100%}.cross_line:hover:after{top:0;right:0;height:100%}';//
	CSS4DISQUS2018+='</style>';//
// Thanks at https://saruwakakun.com/html-css/reference/buttons  !
}//
//	*** Please do not modify the following ***
if(typeof NEWTYPE2018DISQUS == "undefined")	NEWTYPE2018DISQUS = false;//
if(typeof addOnload2018 == "undefined"){//
function addOnload2018(func,ms){if(ms&&eval(ms)>0){try{window.addEventListener("load",setTimeout(function(){func},ms),false)}catch(e){window.attachEvent("onload",setTimeout(function(){func},ms))}}else{try{window.addEventListener("load",func,false)}catch(e){window.attachEvent("onload",func)}}}// Function to be invoked after loading page. It is also possible to "start up after a lapse of time" as an option. Example of use: Execute function "ABC" after 5 seconds after page loading completion:  addOnload2018( ABC , 5000 )
}//
if(location.hostname == MYMAINBLOGURL2018){//
//	In the article on the mirror side, specify "PAGE_URL" and "PAGE_IDENTIFIER" beforehand !
	if(NEWTYPE2018DISQUS)//
		PAGE_URL = 'https://' + location.hostname + location.pathname ;// Non SSL / SSL hybrid support
	else	PAGE_URL = 'http://' + location.hostname + location.pathname ;//
	PAGE_IDENTIFIER = location.pathname.split('/')[1] ;//
//	This script is conscious of WordPress as the main blog, and it is selected as "setting"> "permalink setting"> "common setting": "post name". When setting other, please consider the assignment expression to PAGE_IDENTIFIER!
}
if(typeof _GETELEMENTBYID__disqus_thread_ == "undefined")//
var	_GETELEMENTBYID__disqus_thread_ = document.getElementById('disqus_thread');//
if(typeof WELCOM2THE1STDISQUS2018 == "undefined" && _GETELEMENTBYID__disqus_thread_ ){//	to A
var	WELCOM2THE1STDISQUS2018 = true ;//
	_GETELEMENTBYID__disqus_thread_.insertAdjacentHTML('beforebegin',INSERTADJACENTHTML2018);//
	_GETELEMENTBYID__disqus_thread_.insertAdjacentHTML('afterend',VERSIONOFDISQUS_JS);//
	document.write(CSS4DISQUS2018);//
	CSS4DISQUS2018 = '<noscript><center>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></center></noscript>';//
	if(typeof disqus_config == "undefined"){//	to B
var		disqus_config = function(){//
			function browserLanguage(zz){//
				try {return (navigator.browserLanguage || navigator.language || navigator.userLanguage).substr(0,zz)}//
				catch(e) {return 'en'}//
			}//
var			q = document.cookie.indexOf("googtrans=");//
			if(q >= 0){//
var				qqs = document.cookie.split(';');//
				for( var i = 0 ; i < qqs.length ; i++ ){//
					if( qqs[ i ].indexOf("googtrans=") < 0 ){//
						continue;//
					}//
					q = qqs[ i ].split('/')[2].replace(/ /g,'');//
					break;//
				}//
			}else{	q = browserLanguage(2);//
				switch(q){//
					case"zh":q=browserLanguage(5);break;//China zh-CN:Simplified / zh-TW:Traditional
					case"ce":if(browserLanguage(3)=="ceb"){q="ceb"}break;//Cebuano is a language belonging to the Austronesian language, Hesperonees, Central Philippine languages, Central Filipino group, South Visaya small group.
					case"ha":if(browserLanguage(3)=="haw"){q="haw"}break;//Hawaiian language belongs to the Austronesian language and is the ancestral language of the Hawaiian people who are Polynesians of the Hawaiian islands indigenous people. It is designated as official language of Hawaii State with English.
					case"hm":q=browserLanguage(3);break//Hmong Miao is an ethnic group living in China mostly, the people who speak the same language live in mountainous areas such as Thailand, Myanmar, Laos, Vietnam. Although the self-term is the Hmong, Hmong is a designation used in a narrow sense for one of the Miao tribes. In China it is one of 55 minorities.
				}//
			}//
			this.language = q ;//
			this.page.url = PAGE_URL ;//
			this.page.identifier = PAGE_IDENTIFIER ;//
			q = null ; qqs = null;//
		}//
		function CreateDisqusEmbed2018(z){//
var			d = document, s = d.createElement('script');//
			s.src = 'https://' + z + '.disqus.com/embed.js';//
			s.setAttribute('data-timestamp', +new Date());//
			(d.head || d.body).appendChild(s);//
			s = null;//
var			s = d.createElement('script');//
			s.src = 'https://' + z + '.disqus.com/count.js';//
			s.setAttribute('id','dsq-count-scr');//
			s.setAttribute('async',true);//
			d.body.appendChild(s);//
//			s = null ; //
		}//
//		CreateDisqusEmbed2018( BASEDISQUSHOSTNAME2018 );//
	}//	from B
	(function(){//
var		bottom , targetTop ; //
		window.addEventListener('scroll', this._scrollIn = function(){//
//			The position of the lower end of the display area.
			bottom = window.innerHeight + window.pageYOffset ;//
//			The position of the top of the element.
			targetTop = _GETELEMENTBYID__disqus_thread_.offsetTop ;//
			if(targetTop < bottom){//
				window.removeEventListener('scroll', this._scrollIn , false);//
				CreateDisqusEmbed2018( BASEDISQUSHOSTNAME2018 );//
			}//
		});//
	})();//
}//	from A
/*
[ Revision history ]:
Ver:1.2 2018/09/14
+ window.addEventListener('scroll',
+ function CreateCountJs2018(z){//

Ver:1.1	2018/08/29
+ if(typeof NEWTYPE2018DISQUS == "undefined")	NEWTYPE2018DISQUS = false;//
+ if(NEWTYPE2018DISQUS)//
+	PAGE_URL = 'https://' + location.hostname + location.pathname ;// Non SSL / SSL hybrid support
+ else	PAGE_URL = 'http://' + location.hostname + location.pathname ;//


Ver:1.0 2018/08/20
It avoids errors even in overloading and multiple calling.
- if(typeof CSS4DISQUS2018 == "undefined") CSS4DISQUS2018 = '<noscript><center>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></center></noscript>';//
+ CSS4DISQUS2018 = '<noscript><center>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></center></noscript>';//
*/
